<?php

namespace App\Http\Controllers;

use App\Http\Resources\RestaurantCollection;
use App\Http\Resources\RestaurantResource;
use App\Models\Restaurant;

class RestaurantController extends Controller
{
    public function index()
    {
        return new RestaurantCollection(Restaurant::all());
    }

    public function show(Restaurant $restaurant)
    {
        return new RestaurantResource($restaurant);
    }
}
